import {handleAction} from 'redux-actions';

import {example} from './actions';

const initialState = {};

export default handleAction(example, (state, action) => ({
	...state,
	example: true
}), initialState);
