import {createAction} from 'redux-actions';

const EXAMPLE = 'EXAMPLE';

export const example = createAction(EXAMPLE);
