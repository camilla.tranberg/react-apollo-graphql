import {gql, graphql} from 'react-apollo';
import withData from '../lib/withData';

const App = ({url: {pathname}, data: {loading, allEvents}}) => {
	return (
		<ul>
			{
				allEvents.map((event, i) => {
					return (
						<li key={i}>
							<pre>
								{event.title}
							</pre>
					</li>
					)
				})
			}
		</ul>
	)
};

const allEvents = gql`
  query allEvents {
    allEvents(orderBy: date_ASC) {
      title
    }
  }`;

export default withData(graphql(allEvents)(App));
